# Gnar Patrol

**Part of Project Gnar:** &nbsp;[base](https://hub.docker.com/r/gnar/base) &nbsp;•&nbsp; [gear](https://pypi.org/project/gnar-gear) &nbsp;•&nbsp; [piste](https://gitlab.com/gnaar/piste) &nbsp;•&nbsp; [off-piste](https://gitlab.com/gnaar/off-piste) &nbsp;•&nbsp; [edge](https://www.npmjs.com/package/gnar-edge) &nbsp;•&nbsp; [powder](https://gitlab.com/gnaar/powder)  &nbsp;•&nbsp; [patrol](https://gitlab.com/gnaar/patrol)

Project Gnar is a full stack, turnkey starter web app which includes:

- Sign up email with secure account activation
- Secure login and session management via JWT
- Basic account details (name, address, etc.)
- Password reset email with secure confirmation
- React-based frontend and Python-based microservice backend
- Microservice intra-communication
- SQS message polling and sending
- AWS Cloud-based hosting and Kubernetes deployment

A detailed, step-by-step setup walkthrough will be published on [Medium](https://medium.com) in September 2018 - check back here for the link.

A demo site is up at [gnar.ca](https://app.gnar.ca) - everyone is welcome to create an account and test the workflows.

Gnar Patrol is the set of deployment scripts for the project's Kubernetes cluster.

The Medium article includes a step-by-step walkthough of the deployment.

---

<p><div align="center">Made with <img alt='Love' width='32' height='27' src='https://s3-us-west-2.amazonaws.com/project-gnar/heart-32x27.png'> by <a href='https://www.linkedin.com/in/briengivens'>Brien Givens</a></div></p>
